package mod.puradox.cubicstruct;

import io.github.opencubicchunks.cubicchunks.api.worldgen.CubeGeneratorsRegistry;
import mod.puradox.cubicstruct.command.ReloadCommand;
import mod.puradox.cubicstruct.command.ViewerCommand;
import mod.puradox.cubicstruct.packet.PacketHandler;
import mod.puradox.cubicstruct.packet.ViewerStructureSpawnPacket;
import mod.puradox.cubicstruct.render.RenderHandler;
import mod.puradox.cubicstruct.structure.StructureManager;
import mod.puradox.cubicstruct.worldgen.CubicStructureGenerator;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

@Mod(
        modid = CubicStruct.MOD_ID,
        name = CubicStruct.NAME,
        version = CubicStruct.VERSION,
        dependencies = "required:cubicchunks@[0.0.1244.0,)",
        acceptableRemoteVersions = "*"
)
public class CubicStruct { //Main class
    public static final String MOD_ID = "cubicstruct";
    public static final String NAME = "Cubic Struct";
    public static final String VERSION = "1.2.2";



    @Mod.Instance
    public static CubicStruct instance;

    public static final Logger LOGGER = LogManager.getLogger();
    public static Side side;

    //public static final SimpleNetworkWrapper dispatcher = NetworkRegistry.INSTANCE.newSimpleChannel(CubicStruct.MOD_ID);

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) throws IOException {
        side = event.getSide(); //Server or client.

        StructureManager.initStructures(); //Form directories and .jsons for .nbts placed within the 'cubic_structures' folder, then load them.

        CubeGeneratorsRegistry.register(new CubicStructureGenerator(), 30); //Worldgen hook. Not entirely sure what 'weight' does, unfortunately.

        //dispatcher.registerMessage(PacketHandler.class, ViewerStructureSpawnPacket.class, 0, Side.SERVER); //Packet sent when player attempts to spawn a structure manually.
    }

    @Mod.EventHandler
    public void Init(FMLInitializationEvent event) {
        if(side.isClient()) {
            MinecraftForge.EVENT_BUS.register(new RenderHandler()); //Renders bounding boxes when loading a structure to edit from csviewer.
        }
        //PermissionAPI.registerNode("cubicstruct.spawnStructure", DefaultPermissionLevel.OP, "Whether or not the player can spawn structures.");
    }

    @EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
        event.registerServerCommand(new ReloadCommand()); // /csreload - reinitializes structures.
        if(side.isClient()) {event.registerServerCommand(new ViewerCommand());} // /csviewer - custom gui to preview basic structure information.
    }
}
