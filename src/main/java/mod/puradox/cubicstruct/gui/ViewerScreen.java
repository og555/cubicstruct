package mod.puradox.cubicstruct.gui;

import mod.puradox.cubicstruct.packet.ViewerStructureSpawnPacket;
import mod.puradox.cubicstruct.render.RenderHandler;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;

import mod.puradox.cubicstruct.CubicStruct;
import mod.puradox.cubicstruct.structure.StructureManager;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import org.lwjgl.input.Mouse;

import java.util.ArrayList;
import java.util.List;

import java.io.*;

public final class ViewerScreen extends GuiScreen { //Accessed through command /csviewer. Displays a few attributes of structures, and allows the user to reload them.
    public static final int BACKGROUND_WIDTH = 253;
    public static final int BACKGROUND_HEIGHT = 256;
    public static int CENTRE_X;
    public static int CENTRE_Y;
    public static final int ROWS = 6;

    private int scrollLevel = 0;
    private static int maxScroll = 0;

    private static List<StructureElement> entries;
    private static StructureElement currentEntry; //Entry currently selected.

    private static GuiButton doneButton;
    private static GuiButton refreshButton;

    private static GuiButton spawnButton;
    private static GuiButton cancelSpawnButton;
    private static boolean previewing; //Whether or not a structure preview is visible in-world.

    @Override
    public void initGui() {
        super.initGui();

        CENTRE_X = width / 2 - BACKGROUND_WIDTH /2;
        CENTRE_Y = height / 2 - BACKGROUND_HEIGHT/2;

        initiateButtons(); //Initiate buttons which aren't components of an entry (i.e, 'Done' button)
        initiateEntries(); //Initiate entries from available StructureData objects.

        renderButtons(); //Add buttons to buttonList, which super.drawScreen() will render.
        loadVisibleEntries(); //Draw entries to screen according to scroll.
    }

    private void initiateButtons() {
        if(doneButton != null || refreshButton != null || spawnButton != null || cancelSpawnButton != null) { //To ensure no duplicates.
            buttonList.remove(doneButton);
            buttonList.remove(refreshButton);
            buttonList.remove(spawnButton);
            buttonList.remove(cancelSpawnButton);
        }
        doneButton = new GuiButton(-1, CENTRE_X + 125, CENTRE_Y + 220, 100,20, "Done");
        refreshButton = new GuiButton(-2, CENTRE_X+14, CENTRE_Y + 220, 100,20, "Refresh");
        spawnButton = new GuiButton(-3, CENTRE_X+100, CENTRE_Y + 193, 50,20, "Spawn");
        cancelSpawnButton = new GuiButton(-4, CENTRE_X+175, CENTRE_Y + 193, 50,20, "Cancel");
        //Later may want a setting to auto-import structures to save.
        if(currentEntry != null) {select(currentEntry);} // This should only be called if returning to confirm/deny a placement preview.
    }

    private void initiateEntries() { //Initiate entries from available StructureData objects. Called on screen load/reload.
        scrollLevel = 0;
        if(entries==null) { //Should only be called on initial load.
            entries = new ArrayList<>();
            int[] i = {0};
            StructureManager.structures.forEach(structure -> {
                i[0]++;
                entries.add(new StructureElement(structure, this, i[0], null));
            });
            StructureManager.structureGroups.forEach(group -> group.getStructures().forEach(structure -> {
                i[0]++;
                entries.add(new StructureElement(structure, this, i[0], group.getName()));
            }));
        } else { //Update positions, as if entries aren't null, this was likely called from screen resize.
            entries.forEach(StructureElement::updateScreenPosition);
        }
        maxScroll = Math.max(0,entries.size()-6);
    }

    private void refreshEntries() { //Initiate entries from available StructureData objects. Called on refresh.
        scrollLevel = 0;
        if(entries!=null) {
            //Apparently, setting currentEntry to null, which would be ideal, crashes the game. Is drawScreen() on another thread?
            entries.forEach(entry -> entry.setVisible(false));
            entries = null;
        }

        entries = new ArrayList<>();
        int[] i = {0};
        StructureManager.structures.forEach(structure -> {
            i[0]++;
            entries.add(new StructureElement(structure, this, i[0], null));
        });
        StructureManager.structureGroups.forEach(group -> group.getStructures().forEach(structure -> {
            i[0]++;
            entries.add(new StructureElement(structure, this, i[0], group.getName()));
        }));
        loadVisibleEntries();
        maxScroll = Math.max(0,entries.size()-6);
    }

    private void renderButtons() {
        if(doneButton!=null && refreshButton!=null && spawnButton!=null && cancelSpawnButton!=null){
            buttonList.add(doneButton);
            buttonList.add(refreshButton);
            buttonList.add(spawnButton);
            buttonList.add(cancelSpawnButton);
            if(currentEntry==null) {spawnButton.enabled = false; cancelSpawnButton.visible=false;} else if(!previewing) {cancelSpawnButton.visible=false;}
        } else {
            initiateButtons();
        }
    }

    private void loadVisibleEntries() { //Draw to screen according to scroll.
        entries.forEach(entry -> {
            entry.move(entry.getId()-scrollLevel);
            entry.setVisible(entry.getId() > scrollLevel && entry.getId() <= ROWS+scrollLevel);
            if(entry.isVisible() && !buttonList.contains(entry.getActiveButtons().get(0))) {
                buttonList.addAll(entry.getActiveButtons());
            } else if(!entry.isVisible()) {
                buttonList.removeAll(entry.getActiveButtons());
            }
        });
    }


    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        mc.getTextureManager().bindTexture(new ResourceLocation("cubicstruct", "gui/cubic_gui.png"));
        drawModalRectWithCustomSizedTexture(width / 2 - BACKGROUND_WIDTH /2, height / 2 - BACKGROUND_HEIGHT/2, 0, 0, 253, 256, 253, 256);

        //Draw descriptors.
        if(currentEntry!=null) {
            fontRenderer.drawStringWithShadow(currentEntry.getStructure().getAuthor(), CENTRE_X+100, CENTRE_Y+125, 0x00FFFF);
            fontRenderer.drawStringWithShadow((currentEntry.getGroupName().isEmpty() ? "":currentEntry.getGroupName() + ":") + currentEntry.getStructure().getName(), CENTRE_X + 100, CENTRE_Y + 142, 0x00FFFF);
            fontRenderer.drawStringWithShadow(currentEntry.getStructure().getDescription(), CENTRE_X+100, CENTRE_Y+159, 0x00FFFF);
            fontRenderer.drawStringWithShadow(currentEntry.getStructure().isEnabled() ? "Enabled":"Disabled", CENTRE_X+100, CENTRE_Y+176, currentEntry.getStructure().isEnabled() ? 0x00FF00:0xFF0000);
        }

        //Visualize (may or may not ever be implemented (probably won't))
        // TODO translucent visualization.
        // TODO visualize where surface will be placed according to surfaceLevel.
        // TODO visualize decay with 'new decay seed' button.
        // TODO visualize according to dimension, if only one.
        // TODO visualize according to biome temperature, if only one type of temperature (such as ocean). If all biomes are allowed, somehow indicate this visually.
        // TODO visualize with structure_void toggle.

        //super will draw buttons.
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if(button.id>=0) {
            entries.forEach(entry -> entry.getActiveButtons().forEach(entryButton -> {
                if (button == entryButton) {
                    if (entryButton.equals(entry.getSelectArea())) {
                        select(entry);
                    }
                }
            }));
        } else if (button.id==-1) {
            ConfigManager.sync(CubicStruct.MOD_ID, Config.Type.INSTANCE);
            try {
                StructureManager.initStructures();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            mc.player.closeScreen();
        } else if (button.id==-2) {
            try {
                StructureManager.initStructures();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            refreshEntries();
        } else if (button.id==-3) {
            if (previewing) { //If returning to confirm an existing preview.
                //CubicStruct.dispatcher.sendToServer(new ViewerStructureSpawnPacket(RenderHandler.renderPosition, currentEntry.getStructure()));
                mc.player.sendMessage(new TextComponentString("Feature is currently unimplemented server-side. To prevent client-side abuse, it has been disabled."));
                //currentEntry.getStructure().addBlocksToWorld(mc.world, RenderHandler.renderPosition, currentEntry.getStructure().getSettings());
                setPreviewing(false);
                mc.player.closeScreen();
            } else {
                setPreviewing(true);
                RenderHandler.renderSize=currentEntry.getStructure().getSize();
                RenderHandler.renderPosition=mc.player.getPosition().add(0, currentEntry.getStructure().getSurfaceLevel(), 0);
                mc.player.closeScreen();
            }

        } else if (button.id==-4) {
            if (previewing) { //If returning to deny an existing preview.
                setPreviewing(false);
            } else {
                CubicStruct.LOGGER.warn("You shouldn't be able to click this without a preview present.");
            }
        }
    }

    @Override
    public void handleMouseInput() throws IOException { //Handle scroll.
        super.handleMouseInput();
        int tempScroll = scrollLevel;
        scrollLevel = Math.min(Math.max(scrollLevel - Math.min(Math.max(Mouse.getDWheel(), -1),1), 0), maxScroll);
        if(tempScroll != scrollLevel) {loadVisibleEntries();}
    }

    private void select(StructureElement entry) {
        if (entry.equals(currentEntry)) {return;} //No need to select twice.

        if (currentEntry!=null) {
            setPreviewing(false);
            currentEntry.select(false);
        } //Flush old entry.

        currentEntry=entry;
        currentEntry.select(true); //enable their button (no need to toggle as there is no benefit of removing the information from the screen).

        spawnButton.enabled=true;
        //Render entry contents to the side, after flushing old.
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        if (currentEntry!=null) {
            if(!previewing) {
                currentEntry.select(false);
                currentEntry=null;
                spawnButton.enabled = false;
                cancelSpawnButton.visible = false;
            }
        }
    }

    @Override
    public boolean doesGuiPauseGame() {
        return true;
    }

    public static void setPreviewing(boolean b) {
        previewing = b;
        if(!b) {
            RenderHandler.renderSize=null;
            RenderHandler.renderPosition=null;
            cancelSpawnButton.visible=false;
        }
    }
}
