package mod.puradox.cubicstruct.command;

import mcp.MethodsReturnNonnullByDefault;
import mod.puradox.cubicstruct.structure.StructureManager;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

import javax.annotation.ParametersAreNonnullByDefault;
import java.io.IOException;
@MethodsReturnNonnullByDefault
public class ReloadCommand extends CommandBase {
    private final String COMMAND_NAME = "csreload";
    private final String COMMAND_HELP = "Reload all structures within the cubic_structures directory.";

    @ParametersAreNonnullByDefault
    @Override
    public String getName() {
        return COMMAND_NAME;
    }

    @ParametersAreNonnullByDefault
    @Override
    public String getUsage(ICommandSender sender) {
        return COMMAND_HELP;
    }

    @ParametersAreNonnullByDefault
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) {
        try {
            StructureManager.initStructures();
            sender.sendMessage(new TextComponentString("Structures reloaded successfully."));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
