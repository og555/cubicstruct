package mod.puradox.cubicstruct.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mod.puradox.cubicstruct.structure.StructureData;
import mod.puradox.cubicstruct.structure.StructureGroup;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;

public final class JsonUtils { //Parsing and writing jsons, notably for StructureData.

    private static final Gson GSON = new GsonBuilder().setPrettyPrinting()
            .registerTypeAdapter(StructureData.class, new StructureSerializer())
            .registerTypeAdapter(StructureData.class, new StructureDeserializer())
            .registerTypeAdapter(StructureGroup.class, new GroupSerializer())
            .registerTypeAdapter(StructureGroup.class, new GroupDeserializer())
            .create();
    public static void writeDefaultStructure(File outputLoc) throws FileNotFoundException {
        try (
        FileOutputStream stream = new FileOutputStream(outputLoc);
        Writer writer = new OutputStreamWriter(stream, StandardCharsets.UTF_8)
        ) {
            GSON.toJson(new StructureData(new File(FilenameUtils.removeExtension(String.valueOf(outputLoc))+".nbt")), writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static StructureData readStructureData(File structureJson) throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(structureJson);
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        return GSON.fromJson(bufferedReader, StructureData.class);
    }

    public static StructureGroup readStructureGroup(File structureJson) throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(structureJson);
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        return GSON.fromJson(bufferedReader, StructureGroup.class);
    }
}
