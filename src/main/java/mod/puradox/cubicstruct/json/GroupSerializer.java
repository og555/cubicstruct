package mod.puradox.cubicstruct.json;

import com.google.gson.*;
import mod.puradox.cubicstruct.structure.StructureData;
import mod.puradox.cubicstruct.structure.StructureGroup;

import java.lang.reflect.Type;

public class GroupSerializer implements JsonSerializer<StructureGroup> { //Unused, for now.
    @Override
    public JsonElement serialize(StructureGroup src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray origins = new JsonArray();
        if(!src.getOrigins().isEmpty()) {
            for(StructureData origin: src.getOrigins()) {
                origins.add(origin.getNbtStructure().getName());
            }
        }
        String outName;
        if(src.getName()==null) {
            outName = "Unnamed Group";
        } else {outName = src.getName();}


        JsonObject obj = new JsonObject();
        obj.addProperty("name", outName);
        obj.add("origins", origins);
        obj.addProperty("maxSpreadXZ", src.getMaxSpreadXZ());
        obj.addProperty("maxSpreadY", src.getMaxSpreadY());
        obj.addProperty("minSize", src.getMinSize());
        obj.addProperty("maxSize", src.getMaxSize());
        obj.addProperty("spacing", src.getSpacing());

        return obj;
    }
}
